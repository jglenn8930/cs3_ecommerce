import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';
import Navbar from './components/appnavbars';
import Landing from './pages/Home';
import Products from './pages/Product';
import ProductView from './pages/ProductView';
import Admin from './pages/Admin';
import Create from './pages/CreateProduct'
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Footer from './components/Footer';
import Error from './pages/Error';

import { UserProvider }from './UserContext';

//designated path for each page react-dom
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

// import Product from './components/ProductCard'

export default function App() {
  
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  });
  


  const unsetUser = () => {
    localStorage.clear()

    setUser({
      id: null,
      isAdmin: null
    })

  }

  useEffect(() => {

    console.log(user);

    fetch('https://sheltered-shore-94739.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('accessToken')
        }`
      }
    }).then(resultOfPromise => resultOfPromise.json()).then(convertedResult =>{
      console.log(convertedResult);
      if (convertedResult._id !== "undefined") {
        setUser({
          id: convertedResult._id,
          isAdmin: convertedResult.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
      
    })
  });



  return (
    <UserProvider value={{user, unsetUser, setUser}}>
      <Router>
        <Navbar />

        <Switch>
           <Route exact path="/" component={Landing} />
           <Route exact path="/products" component={Products} />
           <Route exact path="/admin" component={Admin} />
           <Route exact path="/createProduct" component={Create} />
           <Route exact path="/login" component={Login} />
           <Route exact path="/register" component={Register} />
           <Route exact path="/logout" component={Logout} />
           <Route exact path="/products/:productId" component={ProductView} />
           <Route component={Error} />
        </Switch>

       
        <Footer  />
      </Router>
    </UserProvider>

    // <div className="App">
    //     <h1>This is my first React Project </h1>
    //     <h2> My Name is {name} !</h2>
    //     <img src={cartoon} alt="not available" />
    // </div>
    

  );
}


