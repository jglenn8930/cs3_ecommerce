import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

 function Banner({kahitAno}) {

 const { title, description, label } = kahitAno;
	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{description}</p>
				<p>{label}</p>
			</Col>
		</Row>
	)
};

export default Banner;