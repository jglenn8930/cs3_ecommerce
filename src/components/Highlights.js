



import { Row, Col, Card as Baraha } from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
		<Col xs={12} md={4}>
			<Baraha className="Highlights p-3">
				<Baraha.Body>
					<Baraha.Title>
						Learn From Home
					</Baraha.Title>
					<Baraha.Text>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore, blanditiis.
					</Baraha.Text>
				</Baraha.Body>
				
			</Baraha>

		</Col>		

		<Col xs={12} md={4}>
			<Baraha className="Highlights p-3">
				<Baraha.Body>
					<Baraha.Title>
						Study Now Pay Later
					</Baraha.Title>
					<Baraha.Text>
						Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ut velit placeat eius deleniti cupiditate debitis qui, modi natus commodi quia.
					</Baraha.Text>
				</Baraha.Body>
				
			</Baraha>

		</Col>		

		<Col xs={12} md={4}>
			<Baraha className="Highlights p-3">
				<Baraha.Body>
					<Baraha.Title>
						Be Part of Our Community
					</Baraha.Title>
					<Baraha.Text>
						Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ut velit placeat eius deleniti cupiditate debitis qui, modi natus commodi quia.
					</Baraha.Text>
				</Baraha.Body>
				
			</Baraha>

		</Col>		
		</Row>


	)
}