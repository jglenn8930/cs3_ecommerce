import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';





export default function Product({productInfo}) {

const { _id, name, description, price, isActive} = productInfo;	
	return(
		<Card>
			<Card.Body>
			<>
				<Card.Title className="">{name}</Card.Title>
				<Card.Subtitle >Product Description</Card.Subtitle>
					<Card.Text>
						{description}
					</Card.Text>
				<Card.Subtitle>Product Price</Card.Subtitle>
					<Card.Text>
						$ {price}
					</Card.Text>
			</>
				{

				(productInfo.isActive === true) ?	
				<>
				<Card.Subtitle>Available</Card.Subtitle>
					<Card.Text>
						{isActive}
					</Card.Text>
					</>
					:
					<>
				<Card.Subtitle>Not Available</Card.Subtitle>
					<Card.Text>
						{isActive}
					</Card.Text>

					</>
				}
				{
					<Link className="btn btn-primary" to={`/products/${_id}`}> See Details  </Link>
				}
			</Card.Body>
		</Card>
	);
	
}
