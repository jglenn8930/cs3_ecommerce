import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from	'react-bootstrap/Nav';
import { NavLink} from 'react-router-dom';
import UserContext from '../UserContext'

function AppNavbar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>
				Eternal Beauty Brand
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav.Link as={NavLink} to="/">Home</Nav.Link>
				
				

				{(user.id) ?
					<>
					<Nav.Link as={NavLink} to="logout">Logout</Nav.Link>
					<Nav.Link as={NavLink} to="products">Products</Nav.Link>
					
					</>
					:
					<>
					<Nav.Link as={NavLink} to="register">Register</Nav.Link>
					<Nav.Link as={NavLink} to="login">Login</Nav.Link>
					</>
					
				}

				{


				}	


				{(user.isAdmin) ?
					<>
					<Nav.Link as={NavLink} to="admin">Admin
					
					</Nav.Link>
					<Nav.Link as={NavLink} to="createProduct">Create Product</Nav.Link>

					</>
					:
					<>
					</>
				}
				
				
					

				
			</Navbar.Collapse>	
		</Navbar>
		)
}

export default AppNavbar;