import Hero from '../components/Banner';

//named export method
import {Container} from 'react-bootstrap';

let info = {
title : "404 - Page Not Found",
description: "Refresh or Go Back to last page"

};

export default function Error() {
	return (
		<Container>
			<Hero kahitAno = {info} />
				
		</Container>
	);
}