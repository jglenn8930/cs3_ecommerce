import { useState, useEffect } from 'react'
import { Form, Button, Container, } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import {useHistory} from "react-router-dom"

//will use in creating product
let info = {
title : "Add Your Product Here",
description: "Ready, Get set, Go!"
}



export default function Create() {	

	const history = useHistory();
	const [ productName, setProductName ] = useState('');
	const [productDescription , setProductDescription ] = useState('');
	const [productPrice , setProductPrice] = useState('');


	// console.log(firstName)
	const [isRegisterBtnActive, setRegisterBtnActive ] = useState('');
	const [isComplete, setIsComplete] = useState(false);
	// const [isMatched, setIsMatched] = useState(false);

	function registerProduct(e){
	 e.preventDefault();

		console.log(productName);
		console.log(productDescription);
		console.log(productPrice);


 		fetch('https://sheltered-shore-94739.herokuapp.com/products/create', {
		 	method: "POST",
		 	headers: {
	 			'Content-Type' : 'application/json'
	 		},
 		body : JSON.stringify({
	 		name:productName,
	 		description:productDescription,
	 		price:productPrice,
	 	})
 })
 	.then(res => res.json())
 	.then(data => {
 		console.log(data)
 			Swal.fire({
 			title: `Product created successfully!`,
 			icon: 'success',
 			text: 'Your Product Has Been Created!'
 		})
 		history.push('/products');
 	})
 	
};
// 

	// alert('Registered succefully!')
// }

	


useEffect (() => {
	
	if ((productName !== '' && productDescription !== '' && productPrice !== '')) 
	{
		setRegisterBtnActive(true);
		setIsComplete(true);
		
		
	} else {
		setRegisterBtnActive(false);
		setIsComplete(false);
		
	}

	},[productName, productDescription, productPrice]);
	
	return(


	  <Container>

	        {/*Banner of the Page*/}
	   		<Hero kahitAno = {info} />

	   		<img  src="pikachu-cosplay.gif" alt= "not found" />

	   		{
	   			isComplete ?
	   		<h2 className="mb-5"> Create Your Product!</h2>

	   			:
	   		<h2 className="mb-5"> Complete the form to create new product</h2>
	   		}


	   		{/*Login Form Component*/}
			<Form onSubmit={(e) => registerProduct(e)  } className="mb-5">
			    {/*user's first name*/}
				<Form.Group controlId="productName">
				   <Form.Label> Product Name:</Form.Label>
			   <Form.Control type="text" placeholder="Name" value ={productName} onChange= {event => setProductName(event.target.value)} required/>
				</Form.Group>

				{/*user's middle name*/}
				<Form.Group controlId="productDescription">
				   <Form.Label> Describe your product:</Form.Label>
				   <Form.Control type="text" placeholder="Description" value= {productDescription} onChange= {event => setProductDescription(event.target.value)} requried/>
				</Form.Group>
                
                {/*user's last name*/}
				<Form.Group controlId="productPrice">
				   <Form.Label> Price:</Form.Label>
				   <Form.Control type="number" placeholder="Price" value={productPrice} onChange= {event => setProductPrice(event.target.value)} required/>
				</Form.Group>

				{isRegisterBtnActive ?  
                <Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">
                	  Create New Product
                </Button>
                : 
                <Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>
                    Create New Product
                </Button>
              }



			</Form>	   		
	  </Container>

	);
}