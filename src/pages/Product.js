
import {useEffect, useState } from 'react';
import Hero from '../components/Banner';
import SubjectCard from '../components/ProductCard';
import Logout from '../pages/Logout.js'





let info = {
	title: "Welcome to the Product Page!",
	tagline: 'Pick a product!'
}

export default function Products() {

	
	const [products,setProducts] = useState([]);
	
	useEffect(() => {
		fetch('https://sheltered-shore-94739.herokuapp.com/products/active').then(response => response.json()).then(convertedData => {
			console.log(convertedData);

			setProducts(convertedData.map(subject => {
				return(

					<SubjectCard key={subject._id} productInfo={subject} />
				)
			}))
		})
	})

	return (

	<>
		<Hero kahitAno={info} />
		
		{products}
	</>

	
	);
	

}
	<>
	<Logout />
	</>