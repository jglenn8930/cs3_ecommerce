//Purpose: the purpose of this new document is to "unmount" the user from the application and reverting the status of user back into it's default state.
import { useContext, useEffect } from 'react'
//we will also need the capability to redirect the user into another page just incase a NEW user would decide to log in.
import { Redirect } from 'react-router-dom';
//to identify the current state and to eventually change of the user we will need to data provided in the context object.
import UserContext from '../UserContext';

//create a function that will describe the procedure of what will happen if they will log out from the app.
export default function Logout() {

	//consume the "context object" for the user and identify the data that you will need by destructuring the object user. 
    const { unsetUser, setUser, user } = useContext(UserContext) 

    //clear the localStorage property of the browser by invoking the unsetUser method supplied by the provider
    unsetUser(); 

    //redirect the user back into the login.
    //this new module has to RECOGNIZE BY THE APPLICATION.
    //lets create a navbar element for the logout component.
    
    //we need to manage the state of the user upon unmounting

    //lets create a side effect that will allow the Logout page to render first before allowing the changes to made upon the state of the user.
    useEffect(() => {
    	setUser({id: null});
    }) //the component which is a "consumer" is trying the change the context of the user

	return(
		 //para makapag bago siya ng state ni user before redirecting
         //create a page for the logout just to avoid the warning.
        (user.id) ? 
           <h1> Logging Out</h1>
        :
           <Redirect to="/login" />
		);
}
