
import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import {Redirect } from "react-router-dom"
import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner'; 
import Swal from 'sweetalert2';



let info = {
title : "Login",
description: "Welcome Back",
label: "Ready to Book"
};


//create a function that will describe the anatomy of the page.
export default function Login() {

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	// const [isAdmin, setIsAdmin] = useState('');	


	const authenticate = (e) => {
		e.preventDefault()

		fetch('https://sheltered-shore-94739.herokuapp.com/users/login',{
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(ConvertedInfo => {
			console.log(ConvertedInfo)

			if (typeof ConvertedInfo.accessToken !== "undefined") {

				localStorage.setItem('accessToken', ConvertedInfo.accessToken);
				retrieveUserDetails(ConvertedInfo.accessToken);
				Swal.fire({
					title: "Login Successful!",
					icon: "success",
					text: "Welcome!"
				});
			}
			else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your details and try again!"
				});
			}
			
			
		})
	}

	const retrieveUserDetails = (token) => {

		fetch('https://sheltered-shore-94739.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(resultOfPromise => resultOfPromise.json()).then(convertedResult =>{
			console.log(convertedResult);

			setUser({
				id: convertedResult._id,
				isAdmin: convertedResult.isAdmin

			})
		})
	}

	useEffect(()=>{
		if (email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	return(
		(user.id) ? 
		<Redirect to= "/products"/> 
		: 
		(user.isAdmin) ?
		<Redirect to= "/admin"/>
		:

		
		
	    <Container>
	        {/*Banner of the Page*/}
	   		<Hero kahitAno = {info} />

	   		{/*Login Form Component*/}
			<Form className="mb-5" onSubmit={e => authenticate(e)}>
			    {/*user's email*/}
				<Form.Group controlId="userEmail">
				   <Form.Label> Email Address:</Form.Label>
				   <Form.Control type="email" placeholder="Insert Email Here" onChange= {e => setEmail(e.target.value)} value={email} required/>
				</Form.Group>
                
                {/*user's password*/}
				<Form.Group controlId="userPassword">
				   <Form.Label> Password:</Form.Label>
				   <Form.Control type="password" placeholder="Insert Password Here" onChange= {e => setPassword(e.target.value)}required/>
				</Form.Group>

				{/*button component*/}
			{	isActive ?
				<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
				:
				<Button type="submit" id="submitBtn" variant="success" className="btn btn-block" disabled>Login</Button>

			}	
				
			</Form>	   		
	  </Container>


	);


}


 //green === success
 //red === danger 
