
import {useEffect, useState } from 'react';
import Hero from '../components/Banner';
import SubjectCard from '../components/ProductCard';

let info = {
	title: "Welcome Admin!",
	tagline: 'All products!'
}

export default function Products() {
	
	const [products,setProducts] = useState([]);
	
	useEffect(() => {
		fetch('https://sheltered-shore-94739.herokuapp.com/products/').then(response => response.json()).then(convertedData => {
			console.log(convertedData);

			setProducts(convertedData.map(subject => {
				return(

					<SubjectCard key={subject._id} productInfo={subject} />
				)
			}))
		})
	})

	return (

	<>
		<Hero kahitAno={info} />
		{products}
	</>
	);
}