import { useState, useEffect } from 'react'
import { Form, Button, Container, } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import {useHistory} from "react-router-dom"

//will use in creating product
let info = {
title : "Create Your Account Here",
description: "Ready, Get set, Go!"
}



export default function Register() {	

	const history = useHistory();
	const [ firstName, setFirstName ] = useState('');
	const [middleNAme , setMiddleName ] = useState('');
	const [lastName , setLastName] = useState('');
	const [email , setEmail ] = useState('');
	const [gender, setGender ] = useState('Male');
	const [mobileNumber , setMobileNumber ] = useState('');
	const [userPassword , setUserPassword ] = useState('');
	const [confirmPassword , setConfirmPassword ] = useState('');


	// console.log(firstName)
	const [isRegisterBtnActive, setRegisterBtnActive ] = useState('');
	const [isComplete, setIsComplete] = useState(false);
	const [isMatched, setIsMatched] = useState(false);

	function registerUser(e){
	 e.preventDefault();

		console.log(firstName);
		console.log(middleNAme);
		console.log(lastName);
		console.log(gender);
		console.log(email);
		console.log(mobileNumber);
		console.log(userPassword);
		console.log(confirmPassword);


 		fetch('https://sheltered-shore-94739.herokuapp.com/users/register', {
	 	method: "POST",
	 	headers: {
 		'Content-Type' : 'application/json'
 		},
 		body : JSON.stringify({
 		firstName:firstName,
 		middleName:middleNAme,
 		lastName:lastName,
 		email:email,
 		password:userPassword,
 		mobileNo:mobileNumber,
 		gender: gender
 	})
 })
 	.then(res => res.json())
 	.then(data => {
 		console.log(data)
 			Swal.fire({
 			title: `Hey ${data.firstName}, your account was registered successfully!`,
 			icon: 'success',
 			text: 'Your New Account Has Been Created!'
 		})
 		history.push('/login');
 	})
 	
};
// 

	// alert('Registered succefully!')
// }

	


useEffect (() => {
	
	if ((firstName !== '' && middleNAme !== '' && lastName !== '' && email !== '' && userPassword !== '' && confirmPassword !== '' && mobileNumber !== '') && (userPassword === confirmPassword)) 
	{
		setRegisterBtnActive(true);
		setIsComplete(true);
		setIsMatched(true);
		
		
	} else {
		setRegisterBtnActive(false);
		setIsComplete(false);
		setIsMatched(false);
		
	}

	},[firstName, middleNAme, lastName, email, mobileNumber, userPassword, confirmPassword]);
	
	return(


	  <Container>

	        {/*Banner of the Page*/}
	   		<Hero kahitAno = {info} />

	   		<img  src="pikachu-cosplay.gif" alt= "not found" />

	   		{
	   			isComplete ?
	   		<h2 className="mb-5"> Proceed with Register!</h2>

	   			:
	   		<h2 className="mb-5"> Complete the form to create new account</h2>
	   		}


	   		{/*Login Form Component*/}
			<Form onSubmit={(e) => registerUser(e)  } className="mb-5">
			    {/*user's first name*/}
				<Form.Group controlId="firstName">
				   <Form.Label> First Name:</Form.Label>
			   <Form.Control type="text" placeholder="Insert First Name" value ={firstName} onChange= {event => setFirstName(event.target.value)} required/>
				</Form.Group>

				{/*user's middle name*/}
				<Form.Group controlId="middleName">
				   <Form.Label> Middle Name:</Form.Label>
				   <Form.Control type="text" placeholder="Insert Middle Name" value= {middleNAme} onChange= {event => setMiddleName(event.target.value)} requried/>
				</Form.Group>
                
                {/*user's last name*/}
				<Form.Group controlId="lastName">
				   <Form.Label> Last Name:</Form.Label>
				   <Form.Control type="text" placeholder="Insert Last Name" value={lastName} onChange= {event => setLastName(event.target.value)} required/>
				</Form.Group>

				{/*user's age*/}
				<Form.Group controlId="mobileNumber">
				   <Form.Label> Mobile Number:</Form.Label>
				   <Form.Control type="number" placeholder="Insert Number" value={mobileNumber} onInput={e => setMobileNumber (e.target.value = e.target.value.slice(0, 11))} required/>
				</Form.Group>

				{/*user's email*/}

				<Form.Group controlId="userEmail">
				   <Form.Label> Email:</Form.Label>
				   <Form.Control type="email" placeholder="Insert Email Here" value={email} onChange= {e => setEmail(e.target.value)} required/>
				</Form.Group>
				{/*select does not exist as .Notation*/}
				<Form.Group controlId="gender">
				   <Form.Label> Gender:</Form.Label>
				   <Form.Control as="select" value={gender} onChange={(e) => setGender(e.currentTarget.value)}required>
				   		<option selected disabled>Pick Gender</option>
				   		<option value="Male">Male</option>
				   		<option value="Female">Female</option>

				   </Form.Control>
				</Form.Group>

				{/*user's password*/}
				<Form.Group controlId="userPassword">
				   <Form.Label> Password:</Form.Label>
				   <Form.Control type="password" placeholder="Insert Password Here" value={userPassword} onChange= {e => setUserPassword(e.target.value)} required/>
				</Form.Group>

				{
					isMatched ?
						<p className="text-success"> Passwords matched!</p>
					:
						<p className="text-danger"> ****Password should match!**** </p>
				}


				{/*confirm password*/}
				<Form.Group controlId="confirmPassword">
				   <Form.Label> Confirm Password:</Form.Label>
				   <Form.Control type="password" placeholder="Confirm Password Here" value={confirmPassword} onChange= {e => setConfirmPassword(e.target.value)} required/>
				</Form.Group>

				{/*button component*/}
				{isRegisterBtnActive ?  
                <Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">
                	  Create New Account
                </Button>
                : 
                <Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>
                    Create New Account
                </Button>
              }



			</Form>	   		
	  </Container>

	);
}