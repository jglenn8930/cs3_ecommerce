 import { Container, Card, Row, Col } from 'react-bootstrap';
// import { Redirect } from "react-router-dom";
import {  useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';




 export default function ProductView(productInfo) {

   const [name, setName] = useState("");
   const [description, setDescription] = useState("");
   const [price, setPrice] = useState(0);
   const [isActive, setIsActive] =useState("");
   // const { user } = useContext(UserContext);
   // const { _id } = productInfo;
   const { productId } = useParams();

    useEffect(()=> {

      // console.log(_id);

      fetch(`https://sheltered-shore-94739.herokuapp.com/products/${productId}`)
      .then(res => res.json())
      .then(data => {

         console.log(data);

         setName(data.name);
         setDescription(data.description);
         setPrice(data.price);
         setIsActive(data.isActive);

      });

   }, [productId]);

      
        return (


          <Container className="mt-5">
         <Row>
            <Col lg={{ span: 6, offset: 3 }}>
               <Card>
                  <Card.Body className="text-center">
                     <Card.Title>{name}</Card.Title>
                     <Card.Subtitle>Description:</Card.Subtitle>
                     <Card.Text>{description} </Card.Text>
                     <Card.Subtitle>Price:</Card.Subtitle>
                     <Card.Text>{price}</Card.Text>
                     <Card.Subtitle>Available:</Card.Subtitle>
                     <Card.Text>{isActive}</Card.Text>

                      {/*{ (user.id) ? 
                              <Button variant="primary" block onClick={() => enroll(productId)}>Add to Cart</Button>
                                 : 
                              <Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
                      }
         */}

                  
                  </Card.Body>
               </Card>
            </Col>
         </Row>
         
      </Container>
        )
     }



   
      
